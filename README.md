---
description: >-
  Explore, learn, and excel in the dynamic field of translation and localization
  with our Glossary of Translation and Localization Terms.
---

# Glossary of Translation and Localization Terms

Welcome to our comprehensive Glossary of Translation and Localization Terms. This invaluable resource is designed to demystify the complex world of language translation and localization, providing clear definitions and explanations for key terms and concepts.

Whether you're a seasoned professional in the field of translation and localization or just starting to explore this fascinating domain, our glossary is your go-to reference. Inside, you'll find a wealth of information on terminology related to translation techniques, localization strategies, industry standards, and technology tools that play a crucial role in bridging language barriers and connecting global audiences.

### Translation and Localization Terminology



<details>

<summary>Translation</summary>

The process of converting content from one language to another while preserving its meaning and context.

</details>

<details>

<summary>Source Language</summary>

The original language of the content that needs translation or localization.

</details>

<details>

<summary>Target Language</summary>

The language into which the content is being translated or localized.

</details>

<details>

<summary>CAT Tool (Computer-Assisted Translation Tool)</summary>

Software designed to aid translators by suggesting translations and managing translation memories.

</details>

<details>

<summary>Translation Memory</summary>

A database that stores previously translated segments for future use.

</details>

<details>

<summary>Internationalization (i18n)</summary>

Designing software or content to be adaptable to various languages and cultures.

</details>

### Latest Articles from [centus.com](https://centus.com/)

Stay updated with the latest insights from the localization industry by reading the articles from centus.com. Here are some recent posts:

###

<details>

<summary>What is Software Localization: Everything You Need to Know!</summary>

Unlock the world of Software Localization with our comprehensive guide. Discover the essentials and benefits in this must-read article!

Read more: [https://centus.com/blog/what-is-software-localization](https://centus.com/blog/what-is-software-localization)

</details>

<details>

<summary>Market Penetration Examples: Strategies to Scale Your Business</summary>

Learn proven Market Penetration Strategies through real-world examples. Elevate your business growth with our insightful guide.

Read more: [https://centus.com/blog/market-penetration-examples-and-strategies](https://centus.com/blog/market-penetration-examples-and-strategies)

</details>

<details>

<summary>What is Market Expansion Strategy and How to Use It? +Examples</summary>

Explore Market Expansion Strategy and its practical application with real-life examples. Unlock growth opportunities for your business today!

Read more: [https://centus.com/blog/market-expansion-strategy](https://centus.com/blog/market-expansion-strategy)

</details>

<details>

<summary>What Are the Principles of International Marketing?</summary>

Discover the key Principles of International Marketing to excel in the global marketplace. Learn how to expand your reach and succeed internationally.

Read more: [https://centus.com/blog/international-marketing-principles](https://centus.com/blog/international-marketing-principles)

</details>

<details>

<summary>Successful Product Expansion Examples and Strategies to Boost Sales</summary>

Boost your sales with Successful Product Expansion Strategies. Explore real-world examples to supercharge your growth.

Read more: [https://centus.com/blog/product-expansion-examples](https://centus.com/blog/product-expansion-examples)

</details>
